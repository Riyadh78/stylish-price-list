<div class="row">
    <div class="col-md-5 col-lg-5"></div>
    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7 footer-demo-section">
        <?php 
            $opt=get_option('spllk_opt'); 
             if(empty($opt)){
               echo '<p class="free_version">You are using the <span class="highlighted">Demo</span> version of the plugin. Click <span class="highlighted"><a href="http://designful.ca/apps/stylish-price-list-wordpress/">here</a></span> to buy the pro version.</p>';
             }
        ?>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 footer-logo-section">
         <div class="inner-footer-logo-section">  
       <h4>Designed by</h4>
       <a href="http://designful.ca/apps/stylish-price-list-wordpress/" class="spl-footer logo">
            <img src="<?php echo SPL_URL . '/assets/images/logo.png'; ?>" class="img-responsive1" alt="Image" >
       	</a>
       	</div>
    </div>
</div>
<style type="text/css">
    .spl-header{
        display: inline-block;
    }
    .spl-header.logo{
        position: relative;
        top: -20px;
    }
    img.img-responsive1 {
        max-width: 100%;
        height: auto;
    }
    .footer-demo-section .free_version {
    margin: 0px;
    float:right;
}
.footer-logo-section .inner-footer-logo-section {
    margin: 5px 0px;
    float: right;
    clear: both;
}
</style>
