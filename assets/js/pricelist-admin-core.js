var $=jQuery.noConflict();

$('.add_to_webpage').click(function(){
    $('.show_hide_shortcode').toggle();
    $('.font_setting_container').hide();
    $('.more_setting').hide();
});
$('.font_settitng').click(function(){
    $('.font_setting_container').toggle();
    $('.show_hide_shortcode').hide();
    $('.more_setting').hide();
});
$('.advance_setting').click(function(){
	$('.more_setting').toggle();
	$('.font_setting_container').hide();
	$('.show_hide_shortcode').hide();
});
$('.preview_list').click(function(){
	$('#preview_content').toggle();
	$('.backup_content').hide();
	$('.restore_content').hide();
});
$('.backup').click(function(){
	$('#preview_content').hide();
	$('.backup_content').toggle();
	$('.restore_content').hide();
});
$('.restore').click(function(){
	$('#preview_content').hide();
	$('.backup_content').hide();
	$('.restore_content').toggle();
});

var change_lang=$('.change_lang').val();var save_lang=$('.save_lang').val();if(change_lang!==''){if(change_lang=='EN'){var cat_name="Category Name ";var cat_des="Category Description ";var service_name="Service Name ";var service_price="Price ";var service_des="Service Description ";var service_url="Service URL "}
if(change_lang=='SP'){var cat_name="nombre de la categoría";var cat_des="Descripción de categoría ";var service_name="Nombre del Servicio";var service_price="Precio del servicio ";var service_des="Descripción del servicio ";var service_url="URL de servicio"}
if(change_lang=='FR'){var cat_name="Nom de catégorie";var cat_des="description de la catégorie ";var service_name="Nom du service";var service_price="Prix du service ";var service_des="Description du service ";var service_url="URL du service"}
if(change_lang=='DE'){var cat_name="categorie naam";var cat_des="categorie beschrijving ";var service_name="Servicenaam";var service_price="Serviceprijs ";var service_des="Servicebeschrijving ";var service_url="Service URL "}}
else{if($.trim(save_lang)=='EN'){var cat_name="Category Name ";var cat_des="Category Description ";var service_name="Service Name";var service_price="Service Price";var service_des="Service Description ";var service_url="Service URL "}
if($.trim(save_lang)=='SP'){var cat_name="nombre de la categoría";var cat_des="Descripción de categoría ";var service_name="Nombre del Servicio";var service_price="Precio del servicio";var service_des="Descripción del servicio ";var service_url="URL de servicio "}
if($.trim(save_lang)=='FR'){var cat_name="Nom de catégorie";var cat_des="description de la catégorie ";var service_name="Nom du service";var service_price="Prix du service";var service_des="Description du service ";var service_url="URL du service"}
if($.trim(save_lang)=='DE'){var cat_name="categorie naam";var cat_des="categorie beschrijving ";var service_name="Servicenaam";var service_price="Serviceprijs";var service_des="Servicebeschrijving ";var service_url="Service URL"}}
function get_category_id(wrapper_id){var cat_input=$(wrapper_id).find('.category_name');if(cat_input.length>0){var _name=cat_input.last().attr('name');return get_cat_id_from_name(_name)}else{return 0}}
function get_category_count(wrapper_id){var cat_input=$(wrapper_id).find('.category_name');if(cat_input.length>0){return cat_input.length}else{return 0}}
function get_cat_id_from_name(name_string){var match=name_string.match(/category\[(.*?)\]\[name\]/);if(null==match){return null}else{return match[1]}}
function get_service_id_for_add_service_link(add_service_ele){var category_row=get_category_row_from_add_remove_service_link(add_service_ele);var service_name_input=category_row.find('.service-price-length-rows-wrapper .service_name');if(service_name_input.length>0){var _name=service_name_input.last().attr('name');return get_service_id_from_name(_name)}else{return null}}
function get_service_id_from_name(name_string){var match=name_string.match(/category\[(\d+)\]\[(\d+)\]\[service_name\]/);if(null==match){return null}else{return match[2]}}
function generate_category_data(cat_id){var result={name:'category['+cat_id+'][name]',id:'category_'+cat_id+'_name',label:cat_name+'('+cat_id+')'};return result}
function generate_service_data(cat_id,service_id){var result={service_name:{name:'category['+cat_id+']['+service_id+'][service_name]',id:'category_'+cat_id+'_'+service_id+'_service_name',label:service_name+'('+service_id+')'},service_price:{name:'category['+cat_id+']['+service_id+'][service_price]',id:'category_'+cat_id+'_'+service_id+'_service_price',label:service_price+'('+service_id+')'},service_desc:{name:'category['+cat_id+']['+service_id+'][service_desc]',id:'category_'+cat_id+'_'+service_id+'_service_desc',label:service_des+'('+service_id+')'},service_url:{name:'category['+cat_id+']['+service_id+'][service_url]',id:'category_'+cat_id+'_'+service_id+'_service_url',label:service_url+'('+service_id+')'}};return result}
function update_category_row_html(cat_wrapper,cat_id,service_id){var _cat_data=generate_category_data(cat_id);var cat_name_row=cat_wrapper.find('.category-name-row:first');var _label=cat_name_row.find('label');_label.attr('for',_cat_data.id);_label.html(_cat_data.label);var cat_des_row=cat_wrapper.find('.category-description-row:first');var _label1=cat_des_row.find('label');_label1.attr('for','category_'+cat_id+'_description');_label1.html(cat_des+'('+cat_id+')');var _input=cat_name_row.find('input.category_name');_input.attr('name',_cat_data.name);_input.attr('id',_cat_data.id);var _textarea=cat_des_row.find('textarea.category_description');_textarea.attr('name','category['+cat_id+'][description]');_textarea.attr('id','category_'+cat_id+'_description');update_service_rows_html(cat_wrapper.find('.service-price-length-rows-wrapper:last'),cat_id,service_id);return cat_wrapper.find('.category-row').html()}
function update_service_rows_html(service_rows_wrapper,cat_id,service_id){console.log(service_rows_wrapper);var service_rows=service_rows_wrapper.find('.service-price-length');var _service_data=generate_service_data(cat_id,service_id);console.log(_service_data);var _service_name_row=$(service_rows[0]);var _label=_service_name_row.find('label');_label.attr('for',_service_data.service_name.id);_label.html(_service_data.service_name.label);var _input=_service_name_row.find('input.service_name');_input.attr('name',_service_data.service_name.name);_input.attr('id',_service_data.service_name.id);var _service_price_row=$(service_rows[1]);var _label=_service_price_row.find('label');_label.attr('for',_service_data.service_price.id);_label.html(_service_data.service_price.label);var _input=_service_price_row.find('input.service_price');_input.attr('name',_service_data.service_price.name);_input.attr('id',_service_data.service_price.id);var _service_desc_row=$(service_rows[2]);var _label=_service_desc_row.find('label');_label.attr('for',_service_data.service_desc.id);_label.html(_service_data.service_desc.label);var _input=_service_desc_row.find('input.service_desc');_input.attr('name',_service_data.service_desc.name);_input.attr('id',_service_data.service_desc.id);var _service_url_row=$(service_rows[3]);var _label=_service_url_row.find('label');_label.attr('for',_service_data.service_url.id);_label.html(_service_data.service_url.label);var _input=_service_url_row.find('input.service_url');_input.attr('name',_service_data.service_url.name);_input.attr('id',_service_data.service_url.id);return service_rows_wrapper.html()}
function get_cat_id_service_id_from_add_service_link(add_service_ele){var category_row=get_category_row_from_add_remove_service_link(add_service_ele);var _cat_id=get_category_id(category_row);var _service_id=get_service_id_for_add_service_link(add_service_ele);return{service_id:_service_id,cat_id:_cat_id}}
function get_category_row_from_add_remove_service_link(add_service_ele){var category_row=add_service_ele.parent().parent().parent().parent();return category_row}
function get_service_rows_from_add_remove_service_link(remove_service_ele){var category_row=remove_service_ele.parent().parent().parent();return category_row}