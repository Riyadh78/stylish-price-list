=== Stylish Price List. ===
Plugin Name: Stylish Price List
Plugin URL: http://designful.ca/apps/stylish-price-list-wordpress/
Description: Provides a prist list for small businness
Version: 4.1.4
Contributors: Designful
Donate link: 
Tags: Price List, Spas, Salons, Small Businesses, Price Table, Pricing Table, Sylish Price List, Fancy Price List, Salon Price List, Resposive Price List,
Requires at least: 4.0
Tested up to: 4.9.8
Stable tag: 4.9.8
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

A stylish price list For Spas, Salons & Small Businesses. You can easily create price lists for your businesses websites wihtout having to hire a designer.


== Description ==

A stylish looking price list builder designed for for small businesses, retail, spa & salons. Extremely easy to use, lots of styling option, no coding needed. Simply just copy and paste the shortcode on your 
page whenever you're done building the list.
**Please use the demo version before purchasing the pro version to test compatibility with your theme**

== Features ==

<strong>MULTIPLE DESIGN STYLEs</strong>
With over 4+ styles to choose from, you can easily customize the output look of your price list in seconds. Some styles compliment spas and salons, while other compliment restaurants.

<strong>FONT STYLE</strong>
You get to choose the font style, color and type of the heading, category titles and service titles.

<strong>RESPONSIVE</strong>
Stylish Price list is responsive and mobile friendly. SPL will look beautiful on mobile, tablet and laptop devices!

<strong>EASILY ADD TO ANY PAGE</strong>
Some WordPress plugins make it hard for you to add their product to your page. We made it super simple with short-codes!


== Designed For ==

- Spas & Salons
- Restaurants
- Graphic & Website Designers
- Retail businesses
- Massage studios
- Photographers
- Much more


== Video: How to use Stylish Price List ==

[youtube https://www.youtube.com/watch?v=zB6kz2nKxoI]


= Check Out Our Other Services =
- <a href="https://designful.ca/services/ottawa-website-design/">Website Design</a>
- <a href="https://designful.ca/services/poster-flyer-design-services-ottawa/">Graphic Design</a>
- <a href="https://designful.ca/services/seo-ottawa/">SEO Ottawa</a>

= Docs & Support =

You can email us for support: 
Please click the Help tab on plugin for support. You may also email us at info@designful.ca or message us on our FaceBook page: https://www.facebook.com/designfulmultimedia/.

= Recommended Plugins =

No plugins are neccessary for this plugin to work.

== Purchase Pro Version == 

http://designful.ca/apps/stylish-price-list-wordpress/

== Installation ==

1. Upload the entire `stylish-price-list` folder to the `/wp-content/plugins/` directory.
1. Activate the plugin through the 'Plugins' menu in WordPress.

You will find 'Stylish Price List' menu in your WordPress admin panel.

== Frequently Asked Questions ==

Do you have questions or issues with Stylish Price List? Use these support channels appropriately. Email us at info@designful.ca

http://designful.ca/apps/stylish-price-list-wordpress/




== Screenshots ==

http://designful.ca/apps/stylish-price-list-wordpress/
